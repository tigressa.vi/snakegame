// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "SpeedSlow.h"
#include "PlayerPawnBase.h"

// Sets default values
AFoodBase::AFoodBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));

}

// Called when the game starts or when spawned
void AFoodBase::BeginPlay()
{

	Super::BeginPlay();

}

// Called every frame
void AFoodBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

int32 rrandX1(int32 Xmin, int32 Xmax)
{
	return rand() % (Xmax - Xmin + 1) + Xmin;
}

int32 rrandY1(int32 Ymin, int32 Ymax)
{
	return rand() % (Ymax - Ymin + 1) + Ymin;
}

void AFoodBase::AddFoodActor()
{

	int32 X = rrandX1(-600, 600);
	int32 Y = rrandY1(-1350, 1350);
	FVector NewLocation(X, Y, 0);
	FTransform NewTransform(NewLocation);
	AFoodBase* NewFoodElem = NewFoodElem = GetWorld()->SpawnActor<AFoodBase>(FoodElementClass, NewTransform);

}


void AFoodBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(1);
			this->Destroy();

			if (FoodElements.Num() == 0)
			{
				AddFoodActor();
			}
		}
	}
}