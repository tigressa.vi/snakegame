// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedSlow.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
ASpeedSlow::ASpeedSlow()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ASpeedSlow::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedSlow::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedSlow::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		if (IsValid(Snake))
		{
			Snake->SpeedRemove();
			this->Destroy();
		}
	}
}
