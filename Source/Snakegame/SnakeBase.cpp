// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "BlockBase.h"
#include "SpeedSlow.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	ElementSize = 100.f;
	MovementSpeed = 1.f;
	LastMoveDirection = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	Count = 10;
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(3);

}
// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASnakeBase::SpeedAdd()
{
	SetActorTickInterval(MovementSpeed -= 0.1);
}

void ASnakeBase::SpeedRemove()
{
	SetActorTickInterval(MovementSpeed += 0.1);
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}

		if (ElemIndex == Count)
		{
			SpeedAdd();
			AddBlockActor();
			AddSlowActor();
			(Count += 10);
		}
	}
}

int32 rrandX2(int32 Xmin, int32 Xmax)
{
	return rand() % (Xmax - Xmin + 1) + Xmin;
}

int32 rrandY2(int32 Ymin, int32 Ymax)
{
	return rand() % (Ymax - Ymin + 1) + Ymin;
}

void ASnakeBase::AddBlockActor()
{

	int32 X = rrandX2(-460, 460);
	int32 Y = rrandY2(-1080, 1080);
	FVector NewLocation(X, Y, 0);
	FTransform NewTransform(NewLocation);
	ABlockBase* NewBlockElem = NewBlockElem = GetWorld()->SpawnActor<ABlockBase>(BlockElementClass, NewTransform);
	int32 BlockElemNum = BlockElements.Add(NewBlockElem);

	if (BlockElemNum == 5)
	{
	}
}

void ASnakeBase::AddSlowActor()
{

	int32 X = rrandX2(-600, 600);
	int32 Y = rrandY2(-1350, 1350);
	FVector NewLocation(X, Y, 0);
	FTransform NewTransform(NewLocation);
	ASpeedSlow* NewSlowElem = NewSlowElem = GetWorld()->SpawnActor<ASpeedSlow>(SlowElementClass, NewTransform);
	int32 ElemIndex = SlowElements.Add(NewSlowElem);

	if (ElemIndex == 1)
	{
		SetLifeSpan(10);
	}
}


void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;

	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;

	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;

	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	
	SnakeElements[0]->ToggleCollesion();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);

	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollesion();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);

		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}