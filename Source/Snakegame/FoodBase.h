// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "FoodBase.generated.h"

class UStaticMeshComponent;
class AFoodBase;

UCLASS()
class SNAKEGAME_API AFoodBase : public AActor, public IInteractable
{
	GENERATED_BODY()
public:	
	// Sets default values for this actor's properties
	AFoodBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFoodBase> FoodElementClass;

	UPROPERTY()
		TArray<AFoodBase*> FoodElements;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddFoodActor();

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
